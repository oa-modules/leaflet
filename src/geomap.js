/**
 workaround refresh bugfix
 */
window.geomap = {
  show: function (id, coords, precision) {
    if (!coords) coords = [46.7111, 1.791];
    if (!precision) precision = 6;
    function updateLeAfletMap() {
      map.invalidateSize();
    }
    window.setInterval(updateLeAfletMap,1000);
    try {
      var map = L.map(id).setView(coords, precision);
    } catch (e) {
      return false;
    }

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    // prevent leaflet bug
    window.addEventListener("animationstart", updateLeAfletMap);
    window.addEventListener("animationiteration", updateLeAfletMap);
    window.addEventListener("animationend", updateLeAfletMap);

    return map;
  },
};